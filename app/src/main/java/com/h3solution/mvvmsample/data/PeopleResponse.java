package com.h3solution.mvvmsample.data;

import com.google.gson.annotations.SerializedName;
import com.h3solution.mvvmsample.model.People;

import java.util.List;

public class PeopleResponse {
    @SerializedName("results")
    private List<People> peopleList;

    public List<People> getPeopleList() {
        return peopleList;
    }

    public void setPeopleList(List<People> peopleList) {
        this.peopleList = peopleList;
    }
}