package com.h3solution.mvvmsample.base;

import android.content.Context;

import java.util.Observable;

import io.reactivex.disposables.CompositeDisposable;

public class BaseViewModel extends Observable {
    protected Context context;
    protected CompositeDisposable compositeDisposable = new CompositeDisposable();

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
        context = null;
    }
}