package com.h3solution.mvvmsample.base;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import java.util.Observable;
import java.util.Observer;

public abstract class BaseActivity<B extends ViewDataBinding, T extends BaseViewModel>
        extends AppCompatActivity implements Observer {

    protected B dataBinding;
    protected T viewModel;

    protected abstract int layoutId();
    protected abstract void initDataBinding();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dataBinding = DataBindingUtil.setContentView(this, layoutId());
        initDataBinding();
    }

    public void setupObserver(Observable observable) {
        observable.addObserver(this);
    }

    @Override
    protected void onDestroy() {
        viewModel.reset();
        super.onDestroy();
    }
}